# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'mainWindow.ui'
##
## Created by: Qt User Interface Compiler version 6.1.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import *  # type: ignore
from PySide6.QtGui import *  # type: ignore
from PySide6.QtWidgets import *  # type: ignore


class Ui_mainWindow(object):
    def setupUi(self, mainWindow):
        if not mainWindow.objectName():
            mainWindow.setObjectName(u"mainWindow")
        mainWindow.resize(500, 670)
        self.verticalLayoutWidget = QWidget(mainWindow)
        self.verticalLayoutWidget.setObjectName(u"verticalLayoutWidget")
        self.verticalLayoutWidget.setGeometry(QRect(10, 10, 481, 651))
        self.verticalLayout = QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.stateLabel = QLabel(self.verticalLayoutWidget)
        self.stateLabel.setObjectName(u"stateLabel")
        font = QFont()
        font.setPointSize(14)
        self.stateLabel.setFont(font)

        self.horizontalLayout_2.addWidget(self.stateLabel)

        self.stateLineEdit = QLineEdit(self.verticalLayoutWidget)
        self.stateLineEdit.setObjectName(u"stateLineEdit")

        self.horizontalLayout_2.addWidget(self.stateLineEdit)


        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.horizontalLayout_7 = QHBoxLayout()
        self.horizontalLayout_7.setObjectName(u"horizontalLayout_7")
        self.detailsLabel = QLabel(self.verticalLayoutWidget)
        self.detailsLabel.setObjectName(u"detailsLabel")
        self.detailsLabel.setFont(font)

        self.horizontalLayout_7.addWidget(self.detailsLabel)

        self.detailsLineEdit = QLineEdit(self.verticalLayoutWidget)
        self.detailsLineEdit.setObjectName(u"detailsLineEdit")

        self.horizontalLayout_7.addWidget(self.detailsLineEdit)


        self.verticalLayout.addLayout(self.horizontalLayout_7)

        self.horizontalLayout_9 = QHBoxLayout()
        self.horizontalLayout_9.setObjectName(u"horizontalLayout_9")
        self.startTimestampLabel = QLabel(self.verticalLayoutWidget)
        self.startTimestampLabel.setObjectName(u"startTimestampLabel")
        self.startTimestampLabel.setFont(font)

        self.horizontalLayout_9.addWidget(self.startTimestampLabel)

        self.startTimestampCheckBox = QCheckBox(self.verticalLayoutWidget)
        self.startTimestampCheckBox.setObjectName(u"startTimestampCheckBox")

        self.horizontalLayout_9.addWidget(self.startTimestampCheckBox)


        self.verticalLayout.addLayout(self.horizontalLayout_9)

        self.horizontalLayout_8 = QHBoxLayout()
        self.horizontalLayout_8.setObjectName(u"horizontalLayout_8")
        self.endTimestampLabel = QLabel(self.verticalLayoutWidget)
        self.endTimestampLabel.setObjectName(u"endTimestampLabel")
        self.endTimestampLabel.setFont(font)

        self.horizontalLayout_8.addWidget(self.endTimestampLabel)

        self.endTimestampSpinBox = QSpinBox(self.verticalLayoutWidget)
        self.endTimestampSpinBox.setObjectName(u"endTimestampSpinBox")
        self.endTimestampSpinBox.setMinimum(1)
        self.endTimestampSpinBox.setMaximum(1507665886)

        self.horizontalLayout_8.addWidget(self.endTimestampSpinBox)


        self.verticalLayout.addLayout(self.horizontalLayout_8)

        self.horizontalLayout_13 = QHBoxLayout()
        self.horizontalLayout_13.setObjectName(u"horizontalLayout_13")
        self.largeImageLabel = QLabel(self.verticalLayoutWidget)
        self.largeImageLabel.setObjectName(u"largeImageLabel")
        self.largeImageLabel.setFont(font)

        self.horizontalLayout_13.addWidget(self.largeImageLabel)

        self.largeImageComboBox = QComboBox(self.verticalLayoutWidget)
        self.largeImageComboBox.setObjectName(u"largeImageComboBox")

        self.horizontalLayout_13.addWidget(self.largeImageComboBox)


        self.verticalLayout.addLayout(self.horizontalLayout_13)

        self.horizontalLayout_14 = QHBoxLayout()
        self.horizontalLayout_14.setObjectName(u"horizontalLayout_14")
        self.largeImageTextLabel = QLabel(self.verticalLayoutWidget)
        self.largeImageTextLabel.setObjectName(u"largeImageTextLabel")
        self.largeImageTextLabel.setFont(font)

        self.horizontalLayout_14.addWidget(self.largeImageTextLabel)

        self.largeImageTextLineEdit = QLineEdit(self.verticalLayoutWidget)
        self.largeImageTextLineEdit.setObjectName(u"largeImageTextLineEdit")

        self.horizontalLayout_14.addWidget(self.largeImageTextLineEdit)


        self.verticalLayout.addLayout(self.horizontalLayout_14)

        self.horizontalLayout_15 = QHBoxLayout()
        self.horizontalLayout_15.setObjectName(u"horizontalLayout_15")
        self.smallImageLabel = QLabel(self.verticalLayoutWidget)
        self.smallImageLabel.setObjectName(u"smallImageLabel")
        self.smallImageLabel.setFont(font)

        self.horizontalLayout_15.addWidget(self.smallImageLabel)

        self.smallImageComboBox = QComboBox(self.verticalLayoutWidget)
        self.smallImageComboBox.setObjectName(u"smallImageComboBox")

        self.horizontalLayout_15.addWidget(self.smallImageComboBox)


        self.verticalLayout.addLayout(self.horizontalLayout_15)

        self.horizontalLayout_16 = QHBoxLayout()
        self.horizontalLayout_16.setObjectName(u"horizontalLayout_16")
        self.smallImageTextLabel = QLabel(self.verticalLayoutWidget)
        self.smallImageTextLabel.setObjectName(u"smallImageTextLabel")
        self.smallImageTextLabel.setFont(font)

        self.horizontalLayout_16.addWidget(self.smallImageTextLabel)

        self.smallImageTextLineEdit = QLineEdit(self.verticalLayoutWidget)
        self.smallImageTextLineEdit.setObjectName(u"smallImageTextLineEdit")

        self.horizontalLayout_16.addWidget(self.smallImageTextLineEdit)


        self.verticalLayout.addLayout(self.horizontalLayout_16)

        self.horizontalLayout_17 = QHBoxLayout()
        self.horizontalLayout_17.setObjectName(u"horizontalLayout_17")
        self.partyIdLabel = QLabel(self.verticalLayoutWidget)
        self.partyIdLabel.setObjectName(u"partyIdLabel")
        self.partyIdLabel.setFont(font)

        self.horizontalLayout_17.addWidget(self.partyIdLabel)

        self.partyIdLineEdit = QLineEdit(self.verticalLayoutWidget)
        self.partyIdLineEdit.setObjectName(u"partyIdLineEdit")
        self.partyIdLineEdit.setReadOnly(True)

        self.horizontalLayout_17.addWidget(self.partyIdLineEdit)


        self.verticalLayout.addLayout(self.horizontalLayout_17)

        self.horizontalLayout_19 = QHBoxLayout()
        self.horizontalLayout_19.setObjectName(u"horizontalLayout_19")
        self.partySizeLabel = QLabel(self.verticalLayoutWidget)
        self.partySizeLabel.setObjectName(u"partySizeLabel")
        self.partySizeLabel.setFont(font)

        self.horizontalLayout_19.addWidget(self.partySizeLabel)

        self.partySizeSpinBox = QSpinBox(self.verticalLayoutWidget)
        self.partySizeSpinBox.setObjectName(u"partySizeSpinBox")
        self.partySizeSpinBox.setMinimum(0)
        self.partySizeSpinBox.setMaximum(1507665886)
        self.partySizeSpinBox.setValue(0)

        self.horizontalLayout_19.addWidget(self.partySizeSpinBox)


        self.verticalLayout.addLayout(self.horizontalLayout_19)

        self.horizontalLayout_20 = QHBoxLayout()
        self.horizontalLayout_20.setObjectName(u"horizontalLayout_20")
        self.partyMaxLabel = QLabel(self.verticalLayoutWidget)
        self.partyMaxLabel.setObjectName(u"partyMaxLabel")
        self.partyMaxLabel.setFont(font)

        self.horizontalLayout_20.addWidget(self.partyMaxLabel)

        self.partyMaxSpinBox = QSpinBox(self.verticalLayoutWidget)
        self.partyMaxSpinBox.setObjectName(u"partyMaxSpinBox")
        self.partyMaxSpinBox.setMinimum(0)
        self.partyMaxSpinBox.setMaximum(1507665886)
        self.partyMaxSpinBox.setValue(0)

        self.horizontalLayout_20.addWidget(self.partyMaxSpinBox)


        self.verticalLayout.addLayout(self.horizontalLayout_20)

        self.horizontalLayout_22 = QHBoxLayout()
        self.horizontalLayout_22.setObjectName(u"horizontalLayout_22")
        self.joinSecretLabel = QLabel(self.verticalLayoutWidget)
        self.joinSecretLabel.setObjectName(u"joinSecretLabel")
        self.joinSecretLabel.setFont(font)

        self.horizontalLayout_22.addWidget(self.joinSecretLabel)

        self.joinSecretLineEdit = QLineEdit(self.verticalLayoutWidget)
        self.joinSecretLineEdit.setObjectName(u"joinSecretLineEdit")
        self.joinSecretLineEdit.setReadOnly(True)

        self.horizontalLayout_22.addWidget(self.joinSecretLineEdit)


        self.verticalLayout.addLayout(self.horizontalLayout_22)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer_2)

        self.mainWindowExit = QPushButton(self.verticalLayoutWidget)
        self.mainWindowExit.setObjectName(u"mainWindowExit")

        self.horizontalLayout.addWidget(self.mainWindowExit)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)


        self.verticalLayout.addLayout(self.horizontalLayout)


        self.retranslateUi(mainWindow)

        QMetaObject.connectSlotsByName(mainWindow)
    # setupUi

    def retranslateUi(self, mainWindow):
        mainWindow.setWindowTitle(QCoreApplication.translate("mainWindow", u"Discord Custom Rich Presence", None))
        self.stateLabel.setText(QCoreApplication.translate("mainWindow", u"State", None))
        self.detailsLabel.setText(QCoreApplication.translate("mainWindow", u"Details", None))
        self.startTimestampLabel.setText(QCoreApplication.translate("mainWindow", u"Start Timestamp", None))
        self.startTimestampCheckBox.setText(QCoreApplication.translate("mainWindow", u"( Start a timer overrides end timestamp)", None))
        self.endTimestampLabel.setText(QCoreApplication.translate("mainWindow", u"End Timestamp", None))
        self.largeImageLabel.setText(QCoreApplication.translate("mainWindow", u"Large Image", None))
        self.largeImageTextLabel.setText(QCoreApplication.translate("mainWindow", u"Large Image Text", None))
        self.smallImageLabel.setText(QCoreApplication.translate("mainWindow", u"Small Image", None))
        self.smallImageTextLabel.setText(QCoreApplication.translate("mainWindow", u"Small Image Text", None))
        self.partyIdLabel.setText(QCoreApplication.translate("mainWindow", u"Party Id", None))
        self.partyIdLineEdit.setText(QCoreApplication.translate("mainWindow", u"ae488379-351d-4a4f-ad32-2b9b01c91657", None))
        self.partySizeLabel.setText(QCoreApplication.translate("mainWindow", u"Party Size", None))
        self.partyMaxLabel.setText(QCoreApplication.translate("mainWindow", u"Party Max", None))
        self.joinSecretLabel.setText(QCoreApplication.translate("mainWindow", u"Join Secret", None))
        self.joinSecretLineEdit.setText(QCoreApplication.translate("mainWindow", u"MTI4NzM0OjFpMmhuZToxMjMxMjM= ", None))
        self.mainWindowExit.setText(QCoreApplication.translate("mainWindow", u"Close", None))
    # retranslateUi

