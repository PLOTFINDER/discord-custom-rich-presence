import sys
import discord_rpc
import time
from threading import Thread
from PySide6.QtWidgets import QApplication, QMainWindow

from ui_mainWindow import Ui_mainWindow


# pyside6-uic ui/mainWindow.ui > ui_mainWindow.py


class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()

        self.app_id = int(open("application_id", "r").readline())
        self.img_keys = [line.replace("\n", "") for line in open("image_keys.txt", "r").readlines()]

        self.state = None
        self.details = None
        self.start_timestamp = False
        self.last_state_start_timestamp = False
        self.timer = 0
        self.end_timestamp = None
        self.large_image_key = None
        self.large_image_text = None
        self.small_image_key = None
        self.small_image_text = None
        self.party_id = "ae488379-351d-4a4f-ad32-2b9b01c91657"
        self.party_size = None
        self.party_max = None
        self.join_secret = "MTI4NzM0OjFpMmhuZToxMjMxMjM= "

        self.discord_presence = True

        self.discord_presence_thread = Thread(target=self.update_presence).start()

        self.ui = Ui_mainWindow()
        self.ui.setupUi(self)

        self.ui.mainWindowExit.clicked.connect(self.exitMain)
        self.ui.stateLineEdit.textChanged.connect(self.stateValueChanged)
        self.ui.detailsLineEdit.textChanged.connect(self.detailsValueChanged)
        self.ui.startTimestampCheckBox.stateChanged.connect(self.startTimestampStateChanged)
        self.ui.endTimestampSpinBox.valueChanged.connect(self.endTimestampValueChanged)
        self.ui.largeImageComboBox.addItems(self.img_keys)
        self.ui.largeImageComboBox.currentIndexChanged.connect(self.largeImageKeyValueChanged)
        self.ui.largeImageTextLineEdit.textChanged.connect(self.largeImageTextValueChanged)
        self.ui.smallImageComboBox.addItems(self.img_keys)
        self.ui.smallImageComboBox.currentIndexChanged.connect(self.smallImageKeyValueChanged)
        self.ui.smallImageTextLineEdit.textChanged.connect(self.smallImageTextValueChanged)
        self.ui.partyIdLineEdit.textChanged.connect(self.partyIdTextValueChanged)
        self.ui.partySizeSpinBox.valueChanged.connect(self.partySizeValueChanged)
        self.ui.partyMaxSpinBox.valueChanged.connect(self.partyMaxValueChanged)
        self.ui.joinSecretLineEdit.textChanged.connect(self.joinSecretTextValueChanged)

    def stateValueChanged(self):
        text = self.ui.stateLineEdit.text()
        if len(text) >= 2:
            self.state = text
        else:
            self.state = None

    def detailsValueChanged(self):
        text = self.ui.detailsLineEdit.text()
        if len(text) >= 2:
            self.details = text
        else:
            self.details = None

    def startTimestampStateChanged(self):
        self.start_timestamp = self.ui.startTimestampCheckBox.isChecked()
        if self.start_timestamp:
            self.timer = time.time()
        else:
            self.timer = None

    def endTimestampValueChanged(self):
        if self.start_timestamp:
            self.end_timestamp = 0
        else:
            self.end_timestamp = self.ui.endTimestampSpinBox.value()

    def largeImageKeyValueChanged(self):
        key = self.ui.largeImageComboBox.itemText(self.ui.largeImageComboBox.currentIndex())
        if key == "None":
            self.large_image_key = None
        else:
            self.large_image_key = key

    def largeImageTextValueChanged(self):
        text = self.ui.largeImageTextLineEdit.text()
        if len(text) >= 2:
            self.large_image_text = text
        else:
            self.large_image_text = None

    def smallImageKeyValueChanged(self):
        key = self.ui.smallImageComboBox.itemText(self.ui.smallImageComboBox.currentIndex())
        if key == "None":
            self.small_image_key = None
        else:
            self.small_image_key = key

    def smallImageTextValueChanged(self):
        text = self.ui.smallImageTextLineEdit.text()
        if len(text) >= 2:
            self.small_image_text = text
        else:
            self.small_image_text = None

    def partyIdTextValueChanged(self):
        text = self.ui.partyIdLineEdit.text()
        if len(text) >= 2:
            self.large_image_text = text
        else:
            self.large_image_text = None

    def partySizeValueChanged(self):
        value = self.ui.partySizeSpinBox.value()
        if value == 0:
            self.party_size = None
        else:
            self.party_size = value

    def partyMaxValueChanged(self):
        value = self.ui.partyMaxSpinBox.value()
        if value == 0:
            self.party_max = None
        else:
            self.party_max = value

    def joinSecretTextValueChanged(self):
        text = self.ui.joinSecretLineEdit.text()
        if len(text) >= 2:
            self.large_image_text = text
        else:
            self.large_image_text = None

    def exitMain(self):
        self.discord_presence = False
        self.discord_presence_thread.stop()
        self.close()

    def update_presence(self):

        def readyCallback(current_user):
            print('Our user: {}'.format(current_user))

        def disconnectedCallback(codeno, codemsg):
            print('Disconnected from Discord rich presence RPC. Code {}: {}'.format(
                codeno, codemsg
            ))

        def errorCallback(errno, errmsg):
            print('An error occurred! Error {}: {}'.format(
                errno, errmsg
            ))

        callbacks = {
            'ready': readyCallback,
            'disconnected': disconnectedCallback,
            'error': errorCallback,
        }
        discord_rpc.initialize(self.app_id, callbacks=callbacks, log=False)

        while self.discord_presence:
            discord_rpc.update_presence(
                **{
                    'state': self.state,
                    'details': self.details,
                    'start_timestamp': self.timer,
                    'end_timestamp': self.end_timestamp,
                    'large_image_key': self.large_image_key,
                    'large_image_text': self.large_image_text,
                    'small_image_key': self.small_image_key,
                    'small_image_text': self.small_image_text,
                    'party_id': self.party_id,
                    'party_size': self.party_size,
                    'party_max': self.party_max,
                    'join_secret': self.join_secret,
                }
            )

            discord_rpc.update_connection()
            time.sleep(1)
            discord_rpc.run_callbacks()


if __name__ == "__main__":
    app = QApplication(sys.argv)

    window = MainWindow()
    window.show()

    sys.exit(app.exec())
